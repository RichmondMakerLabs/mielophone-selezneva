#! /usr/bin/python
# -*- coding: UTF-8 -*-
from __future__ import division
import os, sys, re

# Assumes SolidPython is in site-packages or elsewhwere in sys.path
from solid import *
from solid.utils import *

SEGMENTS = 48


def assembly():
    # Your code here!
    use("normal_quartz.scad")
    use("trigonal_quartz.scad")

#    scale(0.5)
    d = sphere(60)
    quartz = up(90)(normal_quartz(1.5767,50)) 
    quartz3 = rotate([23,0,0]), quartz
    quartz2 = rotate([0,25,0])up(90)(normal_quartz(1.5767,55))
    a = d + quartz + quartz2 + quartz3

    
    return a

if __name__ == '__main__':
    a = assembly()    
    scad_render_to_file( a, file_header='$fn = %s;'%SEGMENTS, include_orig_code=True)
