 /*
model of a quartz crystal

see The Quartz Page
http://www.quartzpage.de/index.html

Introduction section under Crystals
also
Habits under Crystals

This model shows the Trigonal Habit

see Habits above

size of individual planes can be increased
or decreased by changing last value passed
to mkplane



pcm  May 2015

*/


 trigonal_quartz(1.5767,-2);


module trigonal_quartz(set_angle,length)
{

  /*  set_angle  sets angle between
      m and r faces
      1.5767 gives approximately 141 degrees 45 seconds

       length - length related to length of m faces
       values between -15 and 55 work
       larger positive values make hexagonal prism
       smaller negative numbers a ?
       */


    difference()
      {
  sphere (r=100, center = true);
  union()
      {


 //  m faces

 mkplane( cos(30),sin(30),0,200,200,15);
 mkplane( cos(90),sin(90),0,200,200,15);
 mkplane( cos(150),sin(150),0,200,200,15);
 mkplane( cos(210),sin(210),0,200,200,15);
 mkplane( cos(270),sin(270),0,200,200,15);
 mkplane( cos(330),sin(330),0,200,200,15);



//  r faces top
 translate([0,0,length])
 union()
{
 mkplane(0,2,set_angle,200,200,10);
 mkplane(-sqrt(3),-1,set_angle,200,200,10);
 mkplane(sqrt(3),-1,set_angle,200,200,10);
}



//  r faces bottom
 translate([0,0,-length])
 union()
{
 mkplane(-sqrt(3),1,-set_angle,200,200,10);
 mkplane(0,-2,-set_angle,200,200,10);
 mkplane(sqrt(3),1,-set_angle,200,200,10);
}
}

   }
   }


module mkplane(x2,y2,z2,hthick,r1,r2)
      {
translate(v=[r2*x2,r2*y2,r2*z2])
rotate(a = [-acos(z2/sqrt(x2*x2+y2*y2+z2*z2)), 0, -atan2(x2, y2)])
linear_extrude(height=hthick)
square(r1,center=true);
   }




