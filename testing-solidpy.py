# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 21:43:13 2016

@author: amunizp
"""

from solid import *
from solid.utils import *  # Not required, but the utils module is useful
d = difference()(
    cube(10),
    sphere(15)
)

d = cube(5) + right(5)(sphere(5)) - cylinder(r=2, h=6)

print(scad_render(d))

import os, solid; print(os.path.dirname(solid.__file__) + '/examples')

