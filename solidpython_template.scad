$fn = 48;use </home/andres/mielophone-selezneva/normal_quartz.scad>


union() {
	union() {
		sphere(r = 60);
		translate(v = [0, 0, 90]) {
			normal_quartz(set_angle = 1.5767000000, length = 50);
		}
	}
	translate(v = [0, 0, 90]) {
		normal_quartz(set_angle = 1.5767000000, length = 55);
	}
}
/***********************************************
******      SolidPython code:      *************
************************************************
 
#! /usr/bin/python
# -*- coding: UTF-8 -*-
from __future__ import division
import os, sys, re

# Assumes SolidPython is in site-packages or elsewhwere in sys.path
from solid import *
from solid.utils import *

SEGMENTS = 48


def assembly():
    # Your code here!
    use("normal_quartz.scad")
    use("trigonal_quartz.scad")

#    scale(0.5)
    d = sphere(60)
    rotate([23,0,0])
    quartz = up(90)(normal_quartz(1.5767,50))
    rotate([0,25,0])
    quartz2 = up(90)(normal_quartz(1.5767,55))
    a = d + quartz + quartz2

    
    return a

if __name__ == '__main__':
    a = assembly()    
    scad_render_to_file( a, file_header='$fn = %s;'%SEGMENTS, include_orig_code=True)
 
 
***********************************************/
                            
