 /*
model of a group of quartz needles

see The Quartz Page
http://www.quartzpage.de/index.html

especially the
Introduction under Crystals
also see
Habits under Crystals

This model shows the Normal Habit
also called the Prismatic Habit
or the Maderan habit

it also has a single needle showing
the trigonal habit.

pcm  May 2015

*/

 use <normal_quartz.scad>
 use <trigonal_quartz.scad>



  scale(0.5)
  difference()
  {
  union()
  {
  sphere(r= 60, center=true);
  rotate([23,0,0])
  translate([0,0,90])
  normal_quartz(1.5767,50);
  rotate([0,25,0])
  translate([0,0,90])
  normal_quartz(1.5767,55);
  rotate([-10,-10,0])
 translate([0,0,90])
  trigonal_quartz(1.5767,52);
  rotate([-10,15,0])
  translate([0,0,100])
  normal_quartz(1.5767,55);
  rotate([-10,-15,0])
  translate([0,0,100])
  normal_quartz(1.5767,55);

 }
  translate([0,0,-62])
  cube(125,center=true);
}


